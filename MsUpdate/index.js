const express = require('express')
const bd = require('./conexion-bd')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT || 8000


app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Mysql API' })
});

app.post('/update/:id', (request, response) => {
    const { name, email, phone, birthday, status } = request.body;
    if( name && email && phone && birthday && status)  return bd.updateUser(request, response)
    return response.json({ error: `Valores requeridos name, email, phone, birthday, status` })
});
app.listen(port, () => {
    console.log(`App running on port ${port}.`)
});