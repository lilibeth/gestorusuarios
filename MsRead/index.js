const express = require('express')
const bd = require('./conexion-bd')
const app = express()
const port = process.env.PORT || 8000
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Mysql API' })
});

app.get('/list', (request, response) => {
    return bd.listUsers(request, response)
});
app.get('/user/:id', (request, response) => {
  return bd.listUserId(request, response)
});

app.post('/login', (request, response) => {  
  const { email, password } = request.body 
  if( email && password )  return bd.login(request, response)
  return response.json({ error: `Valores requeridos email && password` })
});
app.listen(port, () => {
    console.log(`App running on port ${port}.`)
});