const express = require('express')
const bd = require('./conexion-bd')
const bodyParser = require('body-parser')
const app = express()
const port = process.env.PORT || 8000


app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Mysql API' })
});

app.get('/delete/:id', (request, response) => {
  return bd.deleteUser(request, response)
});
app.listen(port, () => {
    console.log(`App running on port ${port}.`)
});