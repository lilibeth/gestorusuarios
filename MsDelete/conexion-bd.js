const settings = require("./settings");
const mysql = require("mysql");
const db = mysql.createPool(settings);


const deleteUser = (request, response) => {   
  db.getConnection ( function ( err , connection ) {
      const id = parseInt(request.params.id)
      if (err)  return response.json({ error: `${err}` })   
      console.log("db: preparado"); 
      db.query('DELETE FROM users WHERE id ='+id, (error, results) => {
        if (error)  return response.json({ error: error })  
        response.json({'info': `User deleted with ID: ${id}`})
      })
 }); 
}
   
 
   module.exports = {
    deleteUser
   }
